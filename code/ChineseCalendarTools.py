import yaml
import re
from datetime import datetime, timedelta


def calendar_dict():
    with open('calendarData.yml') as file:
        calendar_data = yaml.load(file)
    return (calendar_data)


calendar_data = calendar_dict()


class LuniSolarCalendar:

    def __init__(self, inputdate):
        """Takes in a string containing a Chinese Lunisolar date, normalise the date input,
        categorize the date according to era (Qing or Republican of China) and set up the
        following attributes of the object: input date (str), norm date (str), period (str),
        nianhao (str), year ganzhi or number (str), lunar month (str), intercal (binary),
        lunar day number/ganzhi (str). It also autocomplete missing information."""

        self.inputdate = inputdate

        try:
            self.normalisation_input()
            self.categorise_period()
            self.date_parsing()
            self.within_lsmonth_validation()

        except Exception:
            print("period is not identified")

    def normalisation_input(self):
        """Called by the __init__() method; takes __self.inputdate__ as input and
        returns a normalised version of the Chinese date."""

        # Normalisation of the Chinese date (by replacing some parts of the date)
        lst_replace = [("朔日", "一日"), ("正月", "一月"), ("腊月", "十二月"), ("廿", "二十"), ("卅", "三十")]
        text = self.inputdate
        for index, value in enumerate(lst_replace):
            text = text.replace(lst_replace[index][0], lst_replace[index][1])

        setattr(self, 'normdate', text)

    def categorise_period(self):
        """Called by the __init__() method; takes __self.normdate__ as input and
        returns the relevant era (Qing or Republic of China), which is stored in __self.period__"""

        if re.search("(天命|天聰|崇德|順治|康熙|雍正|乾隆|嘉慶|道光|咸豐|同治|光緒|宣統)", self.normdate):
            period = "alt_qing"
        elif "民國" in self.normdate:
            period = "minguo"

        setattr(self, 'period', period)

    def extract_nianhao(self, chinesedate_string):
        """Called by the __date_parsing__() method; relies on regrex to set the nianhao property."""
        nianhaoRegrex = calendar_data[self.period]['nianhaoRegrex']
        nianhao = re.search(string=chinesedate_string, pattern=nianhaoRegrex).group() if re.search(
            string=chinesedate_string, pattern=nianhaoRegrex) else str()
        setattr(self, 'nianhao', nianhao)

    def extract_lunar_year(self, chinesedate_string):
        """Called by the __date_parsing__() method;  relies on regrex to set the year ganzhi or year number property."""

        ganzhiRegrex = calendar_data['time_regrex']['ganzhiRegrex']
        yearRegrex = calendar_data['time_regrex']['yearRegrex']

        yearganzhirx = "(?<![月日號])" + ganzhiRegrex
        year_ganzhi = re.search(string=chinesedate_string, pattern=yearganzhirx).group() if re.search(
            string=chinesedate_string, pattern=yearganzhirx) else str()
        setattr(self, 'year_ganzhi', year_ganzhi)

        year_number = re.search(string=chinesedate_string, pattern=yearRegrex).group() if re.search(
            string=chinesedate_string, pattern=yearRegrex) else str()
        year_number = year_number.replace('年', '')
        setattr(self, 'year_number', year_number)

    def complete_year(self):
        """Called by the __date_parsing__() method; autocompletes day ganzhi/year
        number in case of absence of one of the two."""
        if self.year_number == '' and self.year_ganzhi != '':
            # no year number
            number_add = calendar_data[self.period]['dict'][self.nianhao]['ganzhi_year'].index(self.year_ganzhi) + 1
            number_zh = LuniSolarCalendar.returnChineseNumber(number_add)
            setattr(self, 'year_number', number_zh)

        elif self.year_number != '' and self.year_ganzhi == '':
            # no year ganzhi
            ind = LuniSolarCalendar.chin2arab(self.year_number) - 1
            ganzhi_add = calendar_data[self.period]['dict'][self.nianhao]['ganzhi_year'][ind]
            setattr(self, 'year_ganzhi', ganzhi_add)
        # if self.year_number == '' and self.year_ganzhi != '':
        #  year_ind = calendar_data[self.period]['dict'][self.nianhao]['ganzhi_year'].index(self.year_ganzhi) + 1
        #  year = self.returnChineseNumber(year_ind)

    def extract_lunar_month(self, chinesedate_string):
        """Called by the __date_parsing__() method; relies on regrex to set the lunar
        month property."""

        intercal = False

        lmonthRegrex = calendar_data['time_regrex']['lmonthRegrex']

        lmonth = re.search(string=chinesedate_string, pattern=lmonthRegrex).group() if re.search(
            string=chinesedate_string, pattern=lmonthRegrex) else str()

        intercal = True if re.search(string=lmonth, pattern="閏") else False
        setattr(self, 'intercal', intercal)

        lmonth = lmonth.replace('月', '').replace('閏', '') if re.search(string=lmonth, pattern="閏") else lmonth.replace(
            '月', '')
        setattr(self, 'lunar_month', lmonth)

    def extract_lunar_day(self, chinesedate_string):
        """Called by the __date_parsing__() method; relies on regrex to set the
        lunar day number property."""

        ldayRegrex = calendar_data['time_regrex']['ldayRegrex']
        ganzhiRegrex = calendar_data['time_regrex']['ganzhiRegrex']

        lday = re.search(string=chinesedate_string, pattern=ldayRegrex).group().replace('號', '').replace('日',
                                                                                                         '').replace(
            "初", "") if re.search(string=chinesedate_string, pattern=ldayRegrex) else str()
        setattr(self, 'lunar_day', lday)

        dganzhirx = "[月日號]" + ganzhiRegrex
        dayganzhi = re.search(string=chinesedate_string, pattern=dganzhirx).group().replace('號', '').replace('日',
                                                                                                             '').replace(
            "月", "") if re.search(string=chinesedate_string, pattern=dganzhirx) else str()
        setattr(self, 'dayganzhi', dayganzhi)

    def complete_lunar_day(self, chinesedate_string):
        """Called by the __date_parsing__() method; autocompletes lunar day in case
        the day is expressed by 望日, 晦, 除夕 or the day ganzhi"""

        if "望日" in chinesedate_string:
            lday = "十六" if self.lunarmonth_status() else "十五"
            setattr(self, 'lunar_day', lday)

        elif "晦" in chinesedate_string or "除夕" in chinesedate_string:
            lday = "三十" if self.lunarmonth_status() else "二十九"
            setattr(self, 'lunar_day', lday)

        elif self.lunar_day == '' and self.dayganzhi != '':
            converted_day = self.convertDayGangzhi2lunarday()
            setattr(self, 'lunar_day', converted_day)

    def date_parsing(self):
        """Called by the __init()__ method; takes __self.normdate__ as input and uses
        regular expressions to extract and tag parts of the Chinese date: nianhao,
        year ganzhi or number, lunar month, intercal, lunar day number/ganzhi.
        It also autocompletes information of day and year if needed"""

        chinesedate_string = self.normdate

        # Every function captures a specific pattern: nianhao, year ganzhi/number, lunar month, lunar day number/ganzhi
        self.extract_nianhao(chinesedate_string)
        self.extract_lunar_year(chinesedate_string)  # year ganzhi or number
        self.complete_year()
        self.extract_lunar_month(chinesedate_string)
        self.extract_lunar_day(chinesedate_string)  # lunar day number or ganzhi
        self.complete_lunar_day(chinesedate_string)  # calculate lunar day
        self.ganzhi_of_lunar_day()  # calculate the day ganzhi

    def within_lsmonth_validation(self):
        if self.lunar_month != '' and self.lunar_day != '':
            maximum_lday = 30 if self.lunarmonth_status() else 29
            current_lday = self.chin2arab(self.lunar_day)
            valid = current_lday <= maximum_lday
            setattr(self, 'within_lsmonth', valid)
        else:
            setattr(self, 'within_lsmonth', None)

    @staticmethod
    def converting_number(string):

        # Taking in a Chinese number from the Chin2arab function and
        # turning into integer numbers

        # We have two lists of equal lengths.
        # The chin list contains Chinese numbers from one to ten and the arab list has the corresponding numerical number.
        chin = ["元", "一", "二", "两", "三", "四", "五", "六", "七", "八", "九"]
        arab = [1, 1, 2, 2, 3, 4, 5, 6, 7, 8, 9]

        # we look up the position index of a Chinese number in the list.
        ind = chin.index(string)
        result = arab[ind]
        return (result)

    @staticmethod
    def chin2arab(string):

        # Taking in a Chinese number
        # Splitting the number into a unit of an once and a tence
        # turning those variables into numerical numbers and
        # adding them up in the end

        tence = 0
        once = 0

        # Now let's start parsing the input string!
        # If we find a tence in the number,
        if re.search("十", string):
            # we extract that number, store it in the tence variable and remove it from the input string.
            tence = re.match("(.)?十", string).group()
            string = string.replace(tence, '')

            # Then we focus on the newly obtained tence variable.
            # We remove the 十 from the variable
            # and use the rest as input for converting_number()
            # Once the variable is converted, we multiply it by ten.
            # If there was no other Chinese number before 十, we set the value of the variable to ten.
            tence = tence.replace("十", "")
            tence = LuniSolarCalendar.converting_number(tence) * 10 if tence != '' else 10

        # Finally, we convert what is still left in the input string.
        # If there is no value to begin with, we set the value of the variable to zero.
        once = LuniSolarCalendar.converting_number(string) if string != '' else 0
        # We perform an addition of the two variables and return the relevant numerical number of the input string.
        result = tence + once
        return (result)

    def prepare_index_cards(self):

        # The function aims to retrieve the relevant index of year and month needed to retrieve information from the uploaded calendar data

        # we have the year and convert it using LuniSolarCalendar.chin2arab()
        year_ind = LuniSolarCalendar.chin2arab(self.year_number)

        # After having the year index, we determine the month index.

        # we turn chinese string representing month to number using LuniSolarCalendar.chin2arab()

        month = LuniSolarCalendar.chin2arab(self.lunar_month)

        # then we need to know whether we deal with an intercalary month.
        # the resulting index will depend on the scenario:
        # whether lunarsolar date has an or has no intercalary month
        # whether the year of that month has an or no intercalary month
        # whether that month precedes/follows the intercalary month

        # Is there an 閏月 in the year of the input month?
        if "run_閏月" in calendar_data[self.period]['dict'][self.nianhao][year_ind]:

            # Is the date in the (right) intercalary month?
            if self.intercal == True and month == calendar_data[self.period]['dict'][self.nianhao][year_ind]["run_閏月"]:
                month_ind = month

            elif self.intercal == True and month != calendar_data[self.period]['dict'][self.nianhao][year_ind][
                "run_閏月"]:
                raise Exception

            # Does the date happen before or after that month?
            elif month <= calendar_data[self.period]['dict'][self.nianhao][year_ind]["run_閏月"]:
                month_ind = month - 1

            else:
                month_ind = month

        else:
            month_ind = month - 1

        return (year_ind, month_ind)

    def lunarmonth_status(self):
        # This function aims to verify whether the lunar month is 29 days or 30 days

        # get the right indices of year and month
        indices = self.prepare_index_cards()
        year_ind = indices[0]
        month_ind = indices[1]

        # look up the year of the right dynasty
        pref = calendar_data[self.period]['dict'][self.nianhao][year_ind]

        # does the big month list have the self.intercal at all?
        # just in case the list does not have a True key and cannot be extracted.
        if self.intercal not in pref['month_big'].keys():
            # store a nope in the variable
            status_30days = False
            return (status_30days)
        else:
            # see if the month is in the list of big month (true means intercalary, false means usual month)
            lst = pref['month_big'][self.intercal]
            status_30days = month_ind in lst
            return (status_30days)

    def convertDayGangzhi2lunarday(self):
        # To solve the following problem: I want to use the day ganzhi to calculate the lunar day of a particular month
        
        # first we need the index number of the day ganzhi
        # If we have the day ganzhi because of extraction, then we convert it to a number
        GZ_ind = calendar_data['time_data']['ganzhi'].index(self.dayganzhi)
        
        # finding the right indices of year and month
        indices = self.prepare_index_cards()
        year_ind = indices[0]
        month_ind = indices[1]

        # looking up the day ganzhi that is the start of the lunar month in the right year from the relevant ruling period
        # converting the ganzhi to its number counterpart
        start_GZ = calendar_data[self.period]['dict'][self.nianhao][year_ind]['day_ganzhi'][month_ind]
        start_GZ = calendar_data['time_data']['ganzhi'].index(start_GZ)
        
        # then we calculate the final the difference in days between the input day ganzhi and the ganzhi that starts the lunar month
        # We add one day to readjust to the correct day
        day = GZ_ind - start_GZ + 1
        
        if day < 0:
        day = day + 59

        day = self.returnChineseNumber(day)

        return(day)

    @staticmethod
    def returnChineseNumber(inputnumber):
        tence = inputnumber // 10
        once = inputnumber % 10

        chin = ["一", "二", "三", "四", "五", "六", "七", "八", "九"]

        if tence == 0:
            tence_chin = ''
        elif tence == 1:
            tence_chin = '十'
        else:
            tence_chin = chin[tence - 1] + '十'

        if once == 0:
            once_chin = ''
        else:
            once_chin = chin[once - 1]

        return (tence_chin + once_chin)

    def convert_ls_year(self):
        # This function returns the equivalent in Gregorian calendar for the Chinese year.
        # In order to retrieve the right date, we need to determine the year and looked up the date of the first month of that year.

        try:

            indices = self.prepare_index_cards()
            year_ind = indices[0]

            date = calendar_data[self.period]['dict'][self.nianhao][year_ind]['month_start'][0]
            date_object = datetime.strptime(date, '%Y-%m-%d').year
            setattr(self, 'gregorian_year', date_object)
            return (date_object)

        except Exception:
            print('Sorry, date does not exist in the dataset or is incomplete')
            setattr(self, 'gregorian_year', None)
            return (None)

    def convert_ls_month_start(self):
        try:
            indices = self.prepare_index_cards()
            year_ind = indices[0]
            month_ind = indices[1]

            if month_ind < 0:
                raise Exception

            month_start = calendar_data[self.period]['dict'][self.nianhao][year_ind]["month_start"][month_ind]

            setattr(self, 'lmonth_greg_start', month_start)
            return (month_start)

        except Exception:
            print('Sorry, the date does not exist in the dataset or is incomplete')
            return (None)

    def convert_ls_month_end(self):
        try:
            start_gdate = self.convert_ls_month_start()
            start_gdate = datetime.strptime(start_gdate, '%Y-%m-%d')

            if self.lunarmonth_status() == True:
                daydiff = timedelta(days=29)
            else:
                daydiff = timedelta(days=28)

            enddate = start_gdate + daydiff
            return (str(enddate.date()))

        except Exception:
            print('Sorry, the date does not exist in the dataset or is incomplete')
            return (None)

    def convert_ls_date(self):
        # This function is to convert the Chinese date into Gregorian calendar.

        # Using the attributes of the LuniSolarCalendar object, the function retrieves the Gregorian date
        # that corresponds to the first the lunar month of the input Chinese date and adds the number
        # of the days that we need to get the correct gregorian date.
        # The function returns the date as a string

        try:
            indices = self.prepare_index_cards()
            year_ind = indices[0]
            month_ind = indices[1]

            if month_ind < 0:
                raise Exception

            month_start = calendar_data[self.period]['dict'][self.nianhao][year_ind]["month_start"][month_ind]

            day = LuniSolarCalendar.chin2arab(self.lunar_day) - 1

            # Known bug made to be a feature: chin2arab always return 0 by default. If the lunar day is not retrieved, the value of the day variable will be below zero
            if day < 0:
                raise Exception

            daydiff = timedelta(days=day)

            # we turn the beginning date into a datetime object
            date_object = datetime.strptime(month_start, '%Y-%m-%d')

            # adding the day difference to the starting date
            result_date = date_object + daydiff

            result_date = str(result_date.date())

            # And - BOOM -the right day returned
            # (PS: as a STRING, that is!!!)
            setattr(self, 'gregorian_date', result_date)
            return (result_date)

        except Exception:
            print('Sorry, date does not exist in the dataset or is incomplete')
            setattr(self, 'gregorian_date', None)
            return (None)

    @staticmethod
    def number2ganzhi(number):
        ind = number - 1
        gz = calendar_data['time_data']['ganzhi'][ind]
        return (gz)

    @staticmethod
    def ganzhi2number(gz):
        n = calendar_data['time_data']['ganzhi'].index(gz) + 1
        return (n)

    def ganzhi_of_lunar_day(self):

        if self.dayganzhi == '' and self.lunar_day != "":
            # finding the right indices of year and month
            indices = self.prepare_index_cards()
            year_ind = indices[0]
            month_ind = indices[1]

            # looking up the day ganzhi that is the start of the lunar month in the right year from the relevant ruling period
            # converting the ganzhi to its number counterpart
            start_GZ = calendar_data[self.period]['dict'][self.nianhao][year_ind]['day_ganzhi'][month_ind]
            start_GZ = self.ganzhi2number(start_GZ)

            # adding the number of days that comes after the starting ganzhi in order to get the correct day ganzhi
            # Removing the 60 to maintain the ganzhi cyclus.
            days = self.chin2arab(self.lunar_day)
            output_GZ = (start_GZ + days) % 60

            output_GZ = self.number2ganzhi(output_GZ)

            setattr(self, 'dayganzhi', output_GZ)

    @staticmethod
    def gregorianyear2ganZhi(gregorian_year, format='chinese'):
        # re-adjust the time scale
        # remove all full cycles and keep what can't be divided
        # remove 1 for accessing the right item in the pyhton list
        find_ganzhi = ((gregorian_year - 3) % 60)  # thanks wikipedia!

        print('only applicable for years after 4 AD')  # I did not fact-check this => SORRY!

        if format == 'chinese':
            find_ganzhi = LuniSolarCalendar.number2ganzhi(find_ganzhi)
            return (find_ganzhi)
        else:
            return (find_ganzhi)

    @staticmethod
    def find_ganzhi_in_gregorian_calendar(ganzhi, gregorian_year, time="after"):
        # provides the gregorian year of a given ganzhi (input) before/after a certain year.

        starting_point = LuniSolarCalendar.gregorianyear2ganZhi(gregorian_year, format="number")
        ganzhi_number = LuniSolarCalendar.ganzhi2number(ganzhi)

        if time == "after":
            for i in range(0, 61):
                if ((starting_point + i)) > 60:
                    value = starting_point + i - 60
                else:
                    value = starting_point + i

                if value == ganzhi_number:
                    return (gregorian_year + i)

        elif time == "before":
            for i in range(-60, 1):
                if ((starting_point + i)) <= 0:
                    value = (starting_point + i + 60)
                else:
                    value = (starting_point + i)

                if value == ganzhi_number:
                    return (gregorian_year + i)

        else:
            print("wrong input for time parameter")
            raise Exception

    @staticmethod
    def extract_date_from_text(txt):
        # extracts Chinese dates from text with regular expressions.

        qing_nh = calendar_data['alt_qing']["nianhaoRegrex"]
        n = "([元一二三四五六七八九十廿卅]{1,3})"
        gz = calendar_data['time_regrex']['ganzhiRegrex']
        lm = calendar_data['time_regrex']['lmonthRegrex']

        regrx = f'(民國\|清)?{qing_nh}({n}|{gz})年?(岁次{gz})?{lm}?(({n}|{gz}|[望朔晦腊]|除夕)+)?([日號])?'

        lst = []
        for match in re.finditer(regrx, txt, re.S):
            lst.append(match.group())

        return (lst)
