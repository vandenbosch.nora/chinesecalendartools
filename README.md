# ChineseCalendarTools Project

## Description
ChineseCalendarTools provides the code and dataset in order to extract, parse and process historical Lunisolar dates (Qing and Republican Era only). 

It mainly focuses on conversion of Chinese lunar to Gregorian calendar but over time I might add other type of functions that are related to processing of Chinese historical time. I might even plan to come back to this project to include more dynasties. 

Suggestions are always welcome.


## How to use this code?
``` python
# Open up the toolkit after downloading and putting the script and dataset in the right directory
# import ChineseCalendarTools


"""EXAMPLE: converting 清光緒庚子年閏八月庚子"""

# We add the 清光緒庚子年八月卅日 to the input parameter of the class object LuniSolarCalendar.
# LuniSolarCalendar() will call different functions to pre-process the date during the initializing phase.
# The output resulting from this process is stored in the variable example1.

example1 = LuniSolarCalendar("清光緒庚子年閏八月庚子")

# Once set, we can access all properties of the date.

print(example1.__dict__) # OUTPUT: {'inputdate': '清光緒庚子年閏八月庚子', 'normdate': '清光緒庚子年閏八月庚子', 'period': 'alt_qing', 'nianhao': '光緒', 'year_ganzhi': '庚子', 'year_number': '二十六', 'intercal': True, 'lunar_month': '八', 'lunar_day': '一', 'dayganzhi': '庚子', 'within_lsmonth': True}


# Notice that some specific properties that could not be extracted from the input date are autocompleted in the dictionary

print(example1.year_number) # OUTPUT: 二十六

print(example1.lunar_day) # OUTPUT: 一

print(example1.within_lsmonth) # OUTPUT: True (meaning it is within the range of the input lunar month)


# We can also call different functions/methods for this Chinese date,

# like converting date to Gregorian calendar,
print(example1.convert_ls_date()) # OUTPUT: 1900-09-24

# converting year to Gregorian calendar,
print(example1.convert_ls_year()) # OUTPUT: 1900

# retrieving the starting Gregorian date of the input lunar month
print(example1.convert_ls_month_start()) # OUTPUT: 1900-09-24

# And checking whether the lunar month is 30 days (big month)
print(example1.lunarmonth_status()) # False (meaning it is 29 days)


# We can also do additional processing, like

# converting lunar day from Chinese text to integer,
print(LuniSolarCalendar.chin2arab(example1.lunar_day)) # OUTPUT: 1

# and converting day ganzhi from Chinese to number
print(LuniSolarCalendar.ganzhi2number(example1.dayganzhi)) # OUTPUT: 37

"""EXAMPLE: Extracting one date or multiple dates from text"""

doc = "维宣統元年岁次己酉十二月丙子朔，越祭日甲申 。"
for item in LuniSolarCalendar.extract_date_from_text(doc): # WARNING: the funtion assumes certain formats of dates and only minguo/qing!
  print(LuniSolarCalendar(item).__dict__)
  # OUTPUT: {'inputdate': '宣統元年岁次己酉十二月丙子朔', 'normdate': '宣統元年岁次己酉十二月丙子朔', 'period': 'alt_qing', 'nianhao': '宣統', 'year_ganzhi': '己酉', 'year_number': '元', 'intercal': False, 'lunar_month': '十二', 'lunar_day': '一', 'dayganzhi': '丙子', 'within_lsmonth': True}


"""EXAMPLE: ganzhi processing"""

# problem: what is the ganzhi of 1900?
print(LuniSolarCalendar.gregorianyear2ganZhi(1900))

# problem: find Gregorian year for the first 甲子 after 1900
print(LuniSolarCalendar.find_ganzhi_in_gregorian_calendar("甲子", 1900, time = "after")) # OUTPUT: 1924

# problem: find Gregorian year for 甲子 before 1900 (included)
print(LuniSolarCalendar.find_ganzhi_in_gregorian_calendar("甲子", 1900, time = "before")) # OUTPUT: 1864

```

For more information, there is a documentation on the code and the structure of the dataset


## Known problem
- The Kangxi Bug: 
The Kangxi emperor reigned over more than 60 years, meaning that there are two year numbers for the date 康熙壬寅年: 1 and 61
Unfortunely, because of the way I structured the data, the python code will always retrieve information on the first year (and not year 61).
In the future I will fix the bug. In the meantime, keep this problem in mind while processing the dates.

- Calculation of the Gregorian year for a specific ganzhi:
The functions gregorianyear2ganZhi and find_ganzhi_in_gregorian_calendar relies on a formula to do the calculation. However it id only accurate for Gregorian years after 4 AD

## Important note about code or the data
- The original data is extracted from the 兩千年中西曆轉換 from Academia Sinica (https://sinocal.sinica.edu.tw/). If you need to convert dates from other Chinese dynasties, consider their platform as an alternative to this code.

## Project status
Temporarily put on hold (Jan 26, 2022)

