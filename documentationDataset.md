The original data is extracted from the 兩千年中西曆轉換 from Academia Sinica (https://sinocal.sinica.edu.tw/). The dataset only contains chronological information on Qing (1636–1911) and the Republican (1911-1949).


Simplified representation of the hierarical structure in the `calendar data` dictionary
- minguo/qing
    - `dict`
    
      - `nianhao`
    
        * `emperor` (posthumous name emperor)
    
        * `ganzhi_year` (list of ganzhi representing each year within the reign period/nianhao)
    
        * `1, 2, 3, etc.` (each year within the reign period/nianhao)
    
          - list holding starting gregorian dates of every lunar month
          
          - list containing the beginning day ganzhi of every lunar month
    
          - list of which lunar month (integer) is dayue 大月, containing sublist FALSE (the lunar month is not intercalary) and TRUE (intercal)
    - `nianhaoRegrex`
